import io

from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from .models import PictureModel
from .serializers import PictureSerializer, ResizeRequestSerializer


class PictureViewSet(ModelViewSet):
    queryset = PictureModel.objects.all()
    http_method_names = ["get", "post", "delete"]

    # Override
    def get_serializer_class(self):
        if self.action == "resize":
            return ResizeRequestSerializer
        return PictureSerializer

    # Override
    def perform_create(self, serializer):
        image = Image.open(serializer.validated_data["picture"])
        serializer.validated_data["width"] = image.width
        serializer.validated_data["height"] = image.height
        serializer.save()

    @action(detail=True, methods=["post"])
    def resize(self, request, pk=None):
        picture = self.get_object()
        serializer = ResizeRequestSerializer(data=request.data)

        if serializer.is_valid():
            image = Image.open(picture.picture)
            width = serializer.validated_data.get("width", image.width)
            height = serializer.validated_data.get("height", image.height)
            image = image.resize(
                (width, height)
            )
            output = io.BytesIO()
            image.save(output, format="JPEG")
            output.seek(0)

            new_picture = PictureModel(
                name=picture.name + f"resized_{width}_{height}",
                url=picture.url or None,
                picture=InMemoryUploadedFile(
                    output,
                    None,
                    picture.name + f"resized_{width}_{height}",
                    "image/jpeg",
                    image.tell(),
                    None
                ),
                width=width,
                height=height,
                parent_picture=picture
            )
            new_picture.save()
            response = Response(status=201)
            response["Location"] = new_picture.id
            return response
        return Response(status=400)



