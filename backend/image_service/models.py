from django.db import models


class PictureModel(models.Model):
    name = models.CharField(max_length=128)
    url = models.URLField(null=True)
    picture = models.ImageField()
    width = models.IntegerField(blank=True)
    height = models.IntegerField(blank=True)
    parent_picture = models.ForeignKey("self", on_delete=models.CASCADE, null=True)


