from rest_framework import serializers
from .models import PictureModel


class PictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = PictureModel
        fields = '__all__'


class ResizeRequestSerializer(serializers.Serializer):
    width = serializers.IntegerField(allow_null=True, required=False)
    height = serializers.IntegerField(allow_null=True, required=False)

    def validate_width(self, value):
        if not (value is None) and value <= 0:
            raise serializers.ValidationError("Negative width")
        return value

    def validate_height(self, value):
        if not (value is None) and value <= 0:
            raise serializers.ValidationError("Negative height")
        return value
