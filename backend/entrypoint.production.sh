#!/bin/sh
pythom manage.py makemigrations image_service
python manage.py migrate
python manage.py compilemessages
python manage.py collectstatic --noinput
exec "$@"