from rest_framework.routers import DefaultRouter

from image_service import views

router = DefaultRouter()
router.register(r"images", views.PictureViewSet)

